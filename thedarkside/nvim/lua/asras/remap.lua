vim.g.mapleader = " "
-- vim.keymap.set("n", "<leader>V", vim.mode.visual
vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)

vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

vim.keymap.set("n", "J", "mzJ`z")
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")
vim.keymap.set("n", "<leader>o", "<cmd>:only<CR>")


-- greatest remap ever
vim.keymap.set("x", "<leader>p", [["_dP]])

-- next greatest remap ever : asbjornHaland
vim.keymap.set({ "n", "v" }, "<leader>y", [["+y]])
vim.keymap.set("n", "<leader>Y", [["+Y]])
vim.keymap.set({ "n", "v" }, "<leader>d", [["_d]])

vim.keymap.set("i", "<C-c>", "<Esc>")

vim.keymap.set("n", "Q", "<nop>")
-- vim.keymap.set("n", "<C-f>", "<cmd>silent !tmux neww tmux-sessionizer<CR>")


local function format_command()
    if vim.bo.filetype == "zig" then
        local filename = vim.fn.expand("%:p")
        vim.cmd("write")
        vim.cmd("silent !zig fmt " .. filename)
        -- Update buffer
        vim.cmd("edit!")
    else
        vim.lsp.buf.format()
    end
end

-- vim.keymap.set("n", "<leader>f", vim.lsp.buf.format)
vim.keymap.set("n", "<leader>f", format_command)
-- vim.keymap.set("n", "<leader>f", vim.lsp.buf.format)

-- vim.keymap.set("n", "<C-k>", "<cmd>cnext<CR>zz")
-- vim.keymap.set("n", "<C-j>", "<cmd>cprev<CR>zz")
vim.keymap.set("n", "<leader>k", "<cmd>lnext<CR>zz")
vim.keymap.set("n", "<leader>j", "<cmd>lprev<CR>zz")

vim.keymap.set("n", "<leader>s", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])
vim.keymap.set("n", "<leader>x", "<cmd>!chmod +x %<CR>", { silent = true })
vim.keymap.set("n", "<leader>r", "<cmd>!%:p<CR>")


local function run_this_file()
    local filetype = vim.bo.filetype
    if filetype == "python" then
        return "<cmd>write<CR><cmd>!python3 " .. vim.fn.expand("%") .. "<CR>"
    elseif filetype == "rust" then
        return "<cmd>write<CR><cmd>!cargo r<CR>"
    elseif filetype == "cpp" or filetype == "c" then
        return "<cmd>write<CR><cmd>!./run.sh<CR>"
    elseif filetype == "racket" then
        return "<cmd>write<CR><cmd>!racket " .. vim.fn.expand("%") .. "<CR>"
    elseif filetype == "haskell" then
        return "<cmd>write<CR><cmd>!cabal run .<CR>"
    elseif filetype == "zig" then
        return "<cmd>write<CR><cmd>!zig run " .. vim.fn.expand("%") .. "<CR>"
    end
end

vim.keymap.set("n", "<leader>q", run_this_file, { expr = true})
-- vim.keymap.set("n", "<leader>q", "<cmd>!python3 %<CR>")



-- Copilot
-- vim.keymap.set("i", "<C-J>", "<cmd>Copilot next<CR>")
-- vim.keymap.set("i", "<C-K>", "<cmd>Copilot previous<CR>")
-- vim.keymap.set("i", "<C-L>", "<cmd>Copilot suggest<CR>")
vim.keymap.set("i", "<C-J>", "<Cmd>call copilot#Next()<CR>")
vim.keymap.set("i", "<C-K>", "<Cmd>call copilot#Previous()<CR>")
vim.keymap.set("i", "<C-L>", "<Cmd>call copilot#Suggest()<CR>")
vim.keymap.set("i", "<C-I>", "<Cmd>call copilot#Dismiss()<CR>")
-- vim.keymap.set("i", "<C-M>", "<Cmd>call copilot#Accept()<CR>")
vim.keymap.set("i", "<C-O>", "<cmd>Copilot panel<CR>")

local function has_value(tab, val)
    for index, value in ipairs(tab) do
        -- We grab the first index of our sub-table instead
        if value[1] == val then
            return true
        end
    end

    return false
end

local function comment_symbol()
    local slash = { "rust", "c", "c++", "zig", "odin" }
    if has_value(slash, vim.bo.filetype) then
        return "\\/\\/"
    elseif vim.bo.filetype == "python" then
        return "#"
    else
        return "\\/\\/"
    end
end

local function comment_selection()
    local comment = comment_symbol()

    local flipped = false
    local start_line = vim.fn.getpos("v")[2]
    local end_line = vim.fn.getpos(".")[2]
    if (start_line > end_line) then
        start_line, end_line = end_line, start_line
        flipped = true
    end
    local range = start_line .. "," .. end_line

    local cmd = ":" .. range .. "s/\\S/" .. comment .. " &/"
    vim.cmd(cmd)
    if (flipped) then
        local new_pos = vim.fn.getpos("v")
        vim.fn.cursor(start_line, new_pos[3])
    end
end

local function uncomment_selection()
    local comment = comment_symbol()

    local start_line = vim.fn.getpos("v")[2]
    local end_line = vim.fn.getpos(".")[2]
    local flipped = false
    if (start_line > end_line) then
        start_line, end_line = end_line, start_line
        flipped = true
    end
    local range = start_line .. "," .. end_line

    local cmd = ":" .. range .. "s/" .. comment .. " //g"
    vim.cmd(cmd)
    if (flipped) then
        local new_pos = vim.fn.getpos("v")
        vim.fn.cursor(start_line, new_pos[3])
    end
end

vim.keymap.set("v", "<leader>c", comment_selection)
vim.keymap.set("v", "<leader>b", uncomment_selection)


local function comment_line()
    local comment = comment_symbol()

    local cmd = ":s/\\S/" .. comment .. " &/"
    vim.cmd(cmd)
end

local function uncomment_line()
    local comment = comment_symbol()

    local cmd = ":s/" .. comment .. " //g"
    vim.cmd(cmd)
end

vim.keymap.set("n", "<leader>c", comment_line)
vim.keymap.set("n", "<leader>b", uncomment_line)


local function run_command() 
    local filetype = vim.bo.filetype

    if filetype == "rust" then
        return "<cmd>!cargo r<CR>"
    elseif filetype == "python" then
        return "<cmd>!python3 %<CR>"
    elseif filetype == "odin" then
        return "<cmd>!odin run .<CR>"
    elseif filetype == "c" then
        return "<cmd>!sh run.sh<CR>"
    end
end

vim.keymap.set("n", "<leader>q", run_command, { expr = true })

vim.keymap.set("n", "<leader>o", "<cmd>only<CR>")

vim.keymap.set("n", "<C-q>", "<Plug>NetrwRefresh<CR>")


local function edit_header()
    local fname = vim.fn.expand("%:t:r")
    local header = fname .. ".h"
    -- Print header 
    -- print(header)

    local command = "<cmd>e " .. header .. "<CR>"

    return command
end
vim.keymap.set("n", "<leader>h", edit_header, { expr = true })
-- vim.keymap.set("i", "<S-C>", "[[:'<,'>s/\S/\/\/&/]]") 
-- vim.keymap.set("i", "<C-X>", "<cmd>:s#a#//&#<CR>") 
-- vim.keymap.set("i", "<S-S>", "[[:'<,'>s/\/\//]]")
-- vim.keymap.set("i", "<C-x>", "<cmd>:'<,'>s#a#b#g<CR>") 
-- vim.keymap.set("v", "<C-x>", "<cmd>:'<,'>s#a#Q#g<CR>")

-- vim.keymap.set("v", "<C-x>", [[:s/\S/\/\/ &/<CR>]])
-- vim.keymap.set("v", "<C-z>", [[:s/\/\///<CR>]])

local function get_comment_symbol()
   local filetype = vim.bo.filetype
   local comment_symbol = ""
   local comment_symbol_rep = " "
   if filetype == "python" then
       comment_symbol = "#"
       comment_symbol_rep = "#"
    elseif filetype == "typescriptreact" then
        comment_symbol = "//"
        comment_symbol_rep = "\\/\\/"
    elseif filetype == "racket" then
        comment_symbol = ";"
        comment_symbol_rep = ";"
    elseif filetype == "haskell" then
        comment_symbol = "--"
        comment_symbol_rep = "--"
    elseif filetype == "clojure" then
        comment_symbol = ";;"
        comment_symbol_rep = ";;"
    elseif filetype == "sh" then
        comment_symbol = "#"
        comment_symbol_rep = "#"
    else
        comment_symbol = "//"
        comment_symbol_rep = "\\/\\/"
    end

    return comment_symbol, comment_symbol_rep
end

local function toggle_comment_command() 
    local comment_symbol, comment_symbol_rep = get_comment_symbol()

    local vstart = vim.fn.getpos('v') -- [bufnum, lnum, col, off]
    local vend = vim.fn.getpos('.') --

    local line_start = vstart[2]
    local line_end = vend[2]
    if line_start > line_end then
        line_start, line_end = line_end, line_start
    end
    local lines = vim.api.nvim_buf_get_lines(0, line_start - 1, line_end, true)
    local is_commented = false
    for _, line in ipairs(lines) do
        local trimmed = string.gsub(line, "^%s+", "")
        if trimmed:find("^" .. comment_symbol) then
            is_commented = true
            break
        end
    end

    if is_commented then
        local range = line_start .. "," .. line_end
        local cmd = ":" .. range .. "s/" .. comment_symbol_rep .. " //"
        vim.cmd(cmd)
    else
        local range = line_start .. "," .. line_end
        local cmd = ":" .. range .. "s/\\S/" .. comment_symbol_rep .. " &/"
        vim.cmd(cmd)
    end
end


vim.keymap.set("v", "<leader>c", toggle_comment_command)


local function toggle_comment_line() 
   local comment_symbol, comment_symbol_rep = get_comment_symbol()

   local current_line = vim.api.nvim_get_current_line()
   local is_commented = string.gsub(current_line, "^%s+", ""):find("^" .. comment_symbol)
   -- print("Current line is", current_line, "--- Is commented", is_commented, "--- Comment symbol: ", comment_symbol)


   if is_commented then
       local cmd = ":s/" .. comment_symbol_rep .. " //"
       vim.cmd(cmd)
   else
       local cmd = ":s/\\S/" .. comment_symbol_rep .. " &/"
       vim.cmd(cmd)
   end
end


vim.keymap.set("n", "<leader>c", toggle_comment_line)


-- Map NetrwRefresh to allow navigating to right pane in NetrwRefresh
vim.keymap.set("n", "<C-q>", "<Plug>NetrwRefresh<CR>")

local function has_value(tab, val)
    for index, value in ipairs(tab) do
        -- We grab the first index of our sub-table instead
        if value[1] == val then
            return true
        end
    end

    return false
end

vim.keymap.set("n", "-", "@@")

-- vim.keymap.set("t", "<C-Space>", "<C-\\><C-n>")
vim.keymap.set("t", "<Esc>", "<C-\\><C-n>")
