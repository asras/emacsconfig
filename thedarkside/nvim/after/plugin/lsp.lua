local lsp = require('lsp-zero')
lsp.preset('recommended')

lsp.ensure_installed({
    "tsserver",
    "eslint",
    "rust_analyzer",
--    "sumneko_lua",
    "pylsp",
    "jdtls",
    "zls",
})



-- Fix Undefined global 'vim'
-- lsp.configure('sumneko_lua', {
--     settings = {
--         Lua = {
--             diagnostics = {
--                 globals = { 'vim' }
--             }
--         }
--     }
-- })




lsp.configure("zls", {
    settings = {
        enable_autofix = false,
    }
})


lsp.configure('tsserver', {
    settings = {
        typescript = {
            format = {
                insertSpaceAfterOpeningAndBeforeClosingJsxExpressionBraces = true,
                indentSize = 2,
            }
        },
    }
})



lsp.configure("pylsp", {
    settings = {
        pylsp = {
           plugins = {
               pycodestyle = {
            maxLineLength = 200, 
        }
    }
}
}
})
-- pylsp.plugins.flake8.maxComplexity

lsp.configure("pylsp", {
    pylsp = {
        plugins = {
            pycodestyle = {
                ignore = { "E501", "C901" },
            },
            flake8 = {
                maxComplexity = 1000,
            }
        }
    }
})


local cmp = require('cmp')
local cmp_select = { behavior = cmp.SelectBehavior.Select }
local cmp_mappings = lsp.defaults.cmp_mappings({
    ['<C-p>'] = cmp.mapping.select_prev_item(cmp_select),
    ['<C-n>'] = cmp.mapping.select_next_item(cmp_select),
    ['<C-y>'] = cmp.mapping.confirm({ select = true }),
    ['<C-Space>'] = cmp.mapping.complete(),
})


cmp_mappings['<Tab>'] = nil
cmp_mappings['<S-Tab>'] = nil
cmp_mappings['<tab>'] = nil


lsp.set_preferences({
    sign_icons = {},
})

lsp.setup_nvim_cmp({
    mappings = cmp_mappings,
})


lsp.set_preferences({
    suggest_lsp_servers = false,
    sign_icons = {
        error = 'E',
        warn = 'W',
        hint = 'H',
        info = 'I'
    }
})

lsp.on_attach(function(client, bufnr)
    local opts = { buffer = bufnr, remap = false }
    vim.keymap.set("n", "gd", function() vim.lsp.buf.definition() end, opts)
    vim.keymap.set("n", "K", function() vim.lsp.buf.hover() end, opts)
    vim.keymap.set("n", "<leader>vws", function() vim.lsp.buf.workspace_symbol() end, opts)
    vim.keymap.set("n", "<leader>vd", function() vim.diagnostic.open_float() end, opts)
    vim.keymap.set("n", "[d", function() vim.diagnostic.goto_next() end, opts)
    vim.keymap.set("n", "]d", function() vim.diagnostic.goto_prev() end, opts)
    vim.keymap.set("n", "<leader>vca", function() vim.lsp.buf.code_action() end, opts)
    vim.keymap.set("n", "<leader>vrr", function() vim.lsp.buf.references() end, opts)
    vim.keymap.set("n", "<leader>vrn", function() vim.lsp.buf.rename() end, opts)
    vim.keymap.set("i", "<C-h>", function() vim.lsp.buf.signature_help() end, opts)
end)



lsp.setup()
