vim.api.nvim_create_augroup('AutoFormatting', {})

vim.api.nvim_create_autocmd('BufWritePre', {
  pattern = '*.js,*.jsx,*.ts,*.tsx,*.css,*.scss,*.html,*.json,*.md,*.py,*.rs',
  group = 'AutoFormatting',
  callback = function()
       vim.lsp.buf.format({ async = false })
     --  local filetype = vim.bo.filetype
     --  if filetype == "typescript" or filetype == "javascript" or filetype == "typescriptreact" or filetype == "javascriptreact" then 
     --    local filename = vim.fn.expand("%:p")
     --    vim.cmd("write")
     --    vim.cmd("silent !npx prettier --write " .. filename)
     --    vim.cmd("edit!")
     -- else 
     --   vim.lsp.buf.format({ async = false })
     -- end
  end,
})
