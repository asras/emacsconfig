(setq inhibit-startup-message t)

(scroll-bar-mode -1)        ; Disable visible scrollbar
(tool-bar-mode -1)          ; Disable the toolbar
(tooltip-mode -1)           ; Disable tooltips
(set-fringe-mode 10)        ; Give some breathing room

(menu-bar-mode -1)            ; Disable the menu bar

(setq frame-title-format "%b")

(setq ring-bell-function 'ignore)

(add-to-list 'load-path "~/.emacs.d/custom")
(add-to-list 'load-path "~/src/org-mode/lisp")
(require 'python-custom-mode)
(add-to-list 'auto-mode-alist '("\\.py\\'" . python-custom-mode))


;;;; Copilot setup
;; copilot.el must be on load path
;; The node-agent must be configured correctly inside copilot.el, see *copilot-agent-path*
;; in copilot.el
(require 'copilot)
(defun my/copilot-tab ()
  (interactive)
  (or (copilot-accept-completion)
      (indent-for-tab-command)))

(with-eval-after-load 'copilot
  (define-key copilot-mode-map (kbd "<tab>") #'my/copilot-tab))
;;;; 



(require 'cl-lib)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("6b2c6e5bc1e89cf7d927d17f436626eac98a04fdab89e080f4e193f6d291c93d" default))
 '(package-selected-packages
   '(editorconfig rust-mode go-mode simple-httpd projectile general doom-themes doom-modeline counsel ivy-rich rainbow-delimiters which-key swiper cl-lib move-text clojure-mode haskell-mode zenburn-theme julia-mode org use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )


(set-face-attribute 'default nil :font "Fira Code Retina" :height 110)


(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
(setq scroll-step 1 scroll-conservatively 10000)


(require 'package)
(setq package-archives '(
                         ("gnu" . "https://elpa.gnu.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("melpa" . "https://melpa.org/packages/")
                         ; ("melpa-stable" . "https://stable.melpa.org/packages/")
                         ; ("elpa" . "https://elpa.gnu.org/packages/")
                         ))
;; (add-to-list 'package-archives
;;              '("org" . "https://orgmode.org/elpa/") t)
;; (add-to-list 'package-archives
;;              '("melpa-stable" . "https://stable.melpa.org/packages/") t)
;; (add-to-list 'package-archives
;;              '("elpa" . "https://elpa.gnu.org/packages/") t)
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

(column-number-mode)
(global-display-line-numbers-mode t)

(dolist (mode '(org-mode-hook
                term-mode-hook
                shell-mode-hook
                eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

(use-package swiper :ensure t)
(use-package ivy
  :diminish
  :bind (("C-s" . swiper)
         :map ivy-minibuffer-map
         ("TAB" . ivy-alt-done)
         ("C-l" . ivy-alt-done)
         ("C-n" . ivy-next-line)
         ("C-p" . ivy-previous-line)
         :map ivy-switch-buffer-map
         ("C-p" . ivy-previous-line)
         ("C-l" . ivy-done)
         ("C-d" . ivy-switch-buffer-kill)
         :map ivy-reverse-i-search-map
         ("C-p" . ivy-previous-line)
         ("C-d" . ivy-reverse-i-search-kill))
  :config
  (ivy-mode 1))

(use-package go-mode)

(use-package doom-modeline
  :init (doom-modeline-mode 1)
  :custom ((doom-modeline-height 5)))

;; (use-package doom-themes
;;   :init (load-theme 'doom-dracula t))


(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 0.3))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package counsel
  :bind (("M-x" . counsel-M-x)
         ("C-x b" . counsel-ibuffer)
         ("C-x C-f" . counsel-find-file)
         :map minibuffer-local-map
         ("C-r" . 'counsel-minibuffer-history)))

(use-package ivy-rich
  :init
  (ivy-rich-mode 1))

;; Have to run all-the-icons-install-fonts first time
(use-package all-the-icons)

(use-package projectile
  :diminish projectile-mode
  :config (projectile-mode)
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :init
  (when (file-directory-p "~/gits")
    (setq projectile-project-search-path '("~/gits")))
  (setq projectile-switch-project-action #'projectile-dired))


(use-package org)
(defun turn-on-visual-line-mode () (visual-line-mode 1))  
(add-hook 'org-mode-hook 'turn-on-visual-line-mode)


(use-package simple-httpd
  :ensure t)


(setq load-path (cons "~/.emacs.d/glsl/" load-path))
(autoload 'glsl-mode "glsl-mode.el" nil t)
(add-to-list 'auto-mode-alist '("\\.vert\\'" . glsl-mode))
(add-to-list 'auto-mode-alist '("\\.frag\\'" . glsl-mode))

;; Emacs Python Development environment
;; (use-package elpy
;;   :ensure t
;;   :init
;;   (elpy-enable))

;; (setq elpy-rpc-virtualenv-path 'current)
;; (require 'org)
;; (define-key global-map "\C-cl" 'org-store-link)
;; (define-key global-map "\C-ca" 'org-agenda)
;; (setq org-log-done 'time)


;; (load-theme 'wombat)
(load-theme 'doom-zenburn t)
(add-to-list 'auto-mode-alist '("\\.cu\\'" . c++-mode))

(visual-line-mode)

(load (expand-file-name "~/.quicklisp/slime-helper.el"))
(setq inferior-lisp-program "sbcl")


;; Custom navigation functions
(defun my-next-window ()
  (interactive)
  (other-window 1))

(defun my-prev-window ()
  (interactive)
  (other-window -1))


(defun sou ()
  (interactive)
  (scroll-other-window -1))

(defun sod ()
  (interactive)
  (scroll-other-window 1))

(move-text-default-bindings)

(define-key global-map [(meta up)] #'sou)
(define-key global-map [(meta down)] #'sod)

(global-set-key (kbd "M-,") #'my-prev-window)
(global-set-key (kbd "M-.") #'my-next-window)
(global-set-key [M-S-up] 'move-text-up)
(global-set-key [M-S-down] 'move-text-down)
(global-set-key "\C-c\C-d" "\C-a\C- \C-e\M-w\C-m\C-a\C-y\C- \C-e\C-w")


(defvar my-keys-minor-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "M-.") 'my-next-window)
    map)
  "my-keys-minor-mode keymap.")

(define-minor-mode my-keys-minor-mode
  "A minor mode so that my key settings override annoying major modes."
  :init-value t
  :lighter " my-keys")

(my-keys-minor-mode 1)


(defun ends-with-p (string target)
  (string-match-p (concat string "$") target))





(defun asras/revert-selected-buffers-no-confirm (buffers)
  "Revert selected file buffers, without confirmation.
Buffers visiting files that no longer exist are ignored.
Files that are not readable (including do not exist) are ignored.
Other errors while reverting a buffer are reported only as messages."
  (interactive)
  (let (file)
    (dolist (buf buffers)
      (setq file  (buffer-file-name buf))
      (when (and file  (file-readable-p file) (not (eq file (buffer-file-name))))
        (with-current-buffer buf
          (with-demoted-errors "Error: %S" (revert-buffer t t)))))))


(defun apply-to-buffer-filenames (filename-predicate)
  (cl-remove-if-not
   #'(lambda (buf)
       (let ((file (buffer-file-name buf)))
         (and file (funcall filename-predicate file)))) (buffer-list)))

;; (apply-to-buffer-filenames #'(lambda (f) (ends-with-p ".lit" f)))


(defun asras/revert-predicated-buffers-no-confirm (filename-predicate)
  "Revert file buffers for which the predicate returns true, without confirmation.
Buffers visiting files that no longer exist are ignored.
Files that are not readable (including do not exist) are ignored.
Other errors while reverting a buffer are reported only as messages."
  (interactive)
  (asras/revert-selected-buffers-no-confirm (apply-to-buffer-filenames filename-predicate)))

   ;; (cl-remove-if-not
   ;;                                           #'(lambda (buf)
   ;;                                               (let ((file (buffer-file-name buf)))
   ;;                                                 (and file (file-readable-p file)
   ;;                                                      (funcall filename-predicate file)))) (buffer-list))))

;; (cl-remove-if-not #'(lambda (buf) (ends-with-p ".lit" buf)) (buffer-filenames))
;; (cl-remove-if-not #'(lambda (buf) nil) (buffer-list))

(defun asras/revert-all-buffers-no-confirm ()
  "Revert all file buffers, without confirmation.
Buffers visiting files that no longer exist are ignored.
Files that are not readable (including do not exist) are ignored.
Other errors while reverting a buffer are reported only as messages."
  (interactive)
  (asras/revert-selected-buffers-no-confirm (buffer-list)))



;; A list of extra shell commands to run during execution
(defvar *extra-commands* nil)
(defun asras/reset-extras ()
  "Reset extra commands to run during execution"
  (interactive)
  (setq *extra-commands* nil))
(defun asras/add-extra (command-str)
  "Add an extra shell command  to run during execution"
  (interactive "sEnter command: ")
  (push `(lambda () (shell-command ,command-str)) *extra-commands*))

(defun asras/set-single (command-str)
  "Clear extra commands and set this command to be run."
  (interactive "sEnter command: ")
  (asras/reset-extras)
  (push `(lambda () (shell-command ,command-str)) *extra-commands*))


(defvar *mpi-mode* nil)
(defun asras/toggle-mpi-mode ()
  (interactive)
  (setf *mpi-mode* (not *mpi-mode*))
  (if *mpi-mode*
      (message "MPI Mode set to true")
    (message "MPI Mode set to false")))


(defvar *special-command* nil)
(defun asras/set-special-command (command-str)
  (interactive "sEnter command: ")
  (setf *special-command* command-str))

(defun run-special-command ()
  (interactive)
  (if *special-command*
      (shell-command *special-command*)
    (message "No command defined. Use asras/set-special-command")))

;; A function run the current file and a keybind for that function
(defun run-current-file ()
  (interactive)
  (let ((fname (buffer-file-name)))
    (cond
     ((ends-with-p ".py" fname)
      (shell-command (concat "python3 " fname))
      (asras/revert-predicated-buffers-no-confirm #'(lambda (fname) (not (ends-with-p ".py" fname)))))
     
     ((ends-with-p ".cpp" fname)
      (shell-command (concat "g++ -Wall -Wextra -pedantic -o temp.out " fname " && ./temp.out")))

     ((ends-with-p ".c" fname)
      (let ((compiler (if *mpi-mode*
                          "mpicc"
                        "gcc"))
            (run-cmd (if *mpi-mode*
                         "mpirun -np 4"
                         "")))
        (shell-command
         (concat compiler " -Wall -Wextra -pedantic -o temp.out " fname " && " run-cmd " ./temp.out"))))

     ((or (ends-with-p ".lsp" fname) (ends-with-p ".lisp" fname))
      (shell-command (concat "sbcl --script " fname)))

     ((ends-with-p ".hs" fname)
      (shell-command (concat "runhaskell " fname)))

     ((ends-with-p ".lit" fname)
      (shell-command (concat "lit " fname))
      (asras/revert-predicated-buffers-no-confirm #'(lambda (fname) (not (ends-with-p ".lit" fname))))
      (mapcar (lambda (f) (funcall f)) *extra-commands*)
      )

     ((ends-with-p ".sh" fname)
      (shell-command (concat "sh " fname)))

     ((ends-with-p ".md" fname)
      (let (
            (pandoccmd (concat "pandoc " fname " -o " (replace-regexp-in-string ".md" ".html" fname)))
            )
        (shell-command pandoccmd)))

     ((ends-with-p ".rs" fname)
      (shell-command "cargo run"))
     
     (t (message "No behaviour defined")))
    )
  )


(define-key global-map [(meta q)] 'run-current-file)
(define-key global-map [(C q)] 'run-special-command)
